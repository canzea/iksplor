const path = require('path');
const webpack = require('webpack');
//const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCSSExtractPlugin = require('mini-css-extract-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  context: path.resolve(__dirname, 'src'),

//  devtool: 'source-map',

  entry: [
    './index.js'
  ],

  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '',
    filename: 'iksplor.min.js',
    libraryTarget: 'umd'
  },

  externals: {
        "react": "react",
        "react-dom": "react-dom",
        "react-intl": "react-intl",
        "react-router": "react-router",
        "react-router-dom": "react-router-dom",
        "react-bootstrap": "react-bootstrap",
        "react-timeago": "react-timeago",
        "rxjs": "rxjs",
        "mobx": "mobx",
        "mobx-react": "mobx-react",
        "blueimp-tmpl": "blueimp-tmpl",
        //"@material-ui": "@material-ui"
      }
//      /^(rxjs)$/i,
//      /^(@material-ui\/core\/Checkbox)$/i,
  ,

  resolve: {
    modules: [
       path.resolve(__dirname, 'src'),
      'node_modules'
    ],
    extensions: ['.js', '.jsx', '.scss', '.css', '.json'],
    alias: {
      components: path.resolve(__dirname, 'src/components')
    },
  },

  plugins: [

    new MiniCSSExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
//      filename: '[name].css',
//      chunkFilename: '[id].css',
    }),
//    new CleanWebpackPlugin(['dist'], {
//      verbose: false,
//      dry: false
//    }),
//    new webpack.LoaderOptionsPlugin({
//      minimize: true,
//      debug: false
//    }),
//    new webpack.DefinePlugin({
//      __DEVELOPMENT__: false,
//      'process.env.NODE_ENV': JSON.stringify('production')
//    }),

//    new BundleAnalyzerPlugin()
  ],

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          { loader: 'sass-loader' }
        ]
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' }
        ]
      },
      {
        test: /\.(ttf|eot|svg|woff)(\?[a-z0-9]+)?$/,
        use: [
          { loader: 'file-loader?name=[path][name].[ext]' }
        ]
      },
      {
        test: /\.(js|jsx)?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      }
    ]
  }
};
