
# Release Notes


## 0.1.9

- keydata widget type implemented


## 0.2.0

- keydata - generic read-only key/value viewer
- keydata - show the name even if there is no form definition
- keydata - allow table structure to preserve ordering
- allow third-parties to integrate custom widgets
- dynamic picklists based on data in document
- configure readonly, editable, add/remove at different levels
- allow control for expand/collapse
- Type selection for "Add" item to a list
- validation framework
- checkbox and radio support
- for keydata, support a higher level grouping that preserves order
- for keydata, a drag/drop capability
- for keydata, allow levels to be configurable, instead of fixed at 2 levels
