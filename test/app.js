import React from 'react';
import ReactDOM from 'react-dom';
import { NavLink} from 'react-router-dom';

import {Hello} from './hello';

import Wrapper from './wrapper';

const App = (props) => {
  let hel = new Hello();
  let examples = [
    "ecosystem",
    "feature-menus",
    "feature-notes",
    "feature-pan-zoom",
    "home-page",
    "layout-2",
    "layout-3",
    "layout-3b"
  ]
  let links = examples.map(l => (
    <li><a href={"/#/examples/" + l + ""}>Example {l}</a></li>
  ));
  return (
    <div className="row">
        <div className="col-lg-2">
            <ol>
            {links}
            </ol>
        </div>
        <div className="col-lg-10">

            <Wrapper a="b"/>
            {props.children}
        </div>
    </div>
  );
};

export default App;