import React from 'react';
import ReactDOM from 'react-dom';

import * as d3 from 'd3';
import Registry from "../src/Registry";
import {ViewerCanvas} from "../src/ViewerCanvas";

import crossfilter from 'crossfilter';

import Dimension from '../src/utils/Dimension';

class TheWrapper extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {

      var dimensions = [];
      var currentCrossfilter = null;

      function getParameterByName(name, url) {
            if (!url) {
              url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
      }

      this.renderModals();

      let source = getParameterByName('p');
      let self = this;

      let vc = new ViewerCanvas();
      vc.mountNode (self.refs.root);

      vc.on("tabClick", function (e, data, doc) {
          console.log("TAB CLICKED: " + JSON.stringify(data));
      });

      vc.on("shapeClick", function (e, data, doc) {
          console.log("SHAPE CLICK: " + JSON.stringify(doc));
          //data.selected = !data.selected;
          //doc.nodes[0].label = doc.nodes[0].label + 'x';
          //doc.nodes[0].values[0].selected = !data.selected;
          //doc.nodes[0].index = null;
          //doc.nodes.splice(i, 1);
          //let n = doc.nodes.pop();
          //vc.update();

          data.selected = !data.selected;
          //doc.nodes.push(n);

          // Re-calculate list of selected values
          // Re-run the filter
          // Update all the list data (i.e./ counts)

          // Need to use observables for mapping from one dataset to another
          dimensions.forEach (function (dim) {
            dim.applyFilter();
            dim.updateShape();
          });

//          d3.selectAll(doc.nodes).filter(function (e) { return e.type == 'box'; })
//                .remove();
          doc.nodes = doc.nodes.filter(item => item.type != 'circle');
          doc.nodes.forEach (function (e) {
             //e.index = 100+ e.index;
             //e.index = null;
          })
          vc.update();

          let links = [];
          let x1 = dimensions[0].values(201);
          if (x1.length > 200) {
            alert("Only rendering max 200 records.");
          }
          let xOffset = 0;
          let yOffset = 0;
          x1.forEach (function (dd, i) {
                dd.type = "circle";
                dd.boundary = 'A';
                dd.x = (xOffset*50) + 300;
                dd.y = 60 + (30*yOffset);
                if (xOffset == 15) {
                    xOffset = 0;
                    yOffset++;
                }
                xOffset++;
                dd.fixed = true;
                dd.name = "I"+i;
                doc.nodes.push(dd);
                links.push({"source":"B", "target":dd.name, "type":"linear"})

          });
          doc.links = [];

//          paymentsByType.filter(function(type) { return data.selected && type == data.id; }); // selects payments whose total is odd
          //console.log("Data = "+JSON.stringify(paymentsByType.top(Infinity)));
          //vc.setState(doc);
          vc.update();

          doc.links = links;
          vc.update();

          //console.log("SHAPE CLICKED: " + data.content.url);
          //window.location.href = data.content.url;
      });

      vc.on("shapeNavClick", function (e, data, doc) {
          console.log("SHAPE NAV CLICKED: " + JSON.stringify(data));
          $("#myModal").modal();
          vc.trigger("closeNavigation");
      });

      vc.on("change", function (e, data, doc) {
          if (typeof(Storage) !== "undefined") {
             localStorage.setItem(source, JSON.stringify(doc));
          }
      })

      window.onresize = function(event) {
          vc.refreshSize();
      };

      if (localStorage.getItem(source) != null && getParameterByName('f') != "true") {
          vc.setState(JSON.parse(localStorage.getItem(source)));
          vc.render();
          vc.refreshSize();
      } else if (source == null) {
          vc.setState({"nodes":[], "links":[], "zones":[]});
          vc.render();
          vc.refreshSize();
          alert("No source passed in as query 'p'");
      } else {

          d3.csv("examples/shape-list.csv", function (json) {
              let payments = crossfilter(json);

              currentCrossfilter = payments;

              let dimmy = new Dimension (payments, function (d) { return d.type; });
              let dimmy2 = new Dimension (payments, function (d) { return d.tip <  100 ? "Low":"High"; });
              dimensions.push(dimmy);
              dimensions.push(dimmy2);

              let newData = dimmy.buildShape("Type");
              let newData2 = dimmy2.buildShape("High-Low");

              let links = [];

              // The payments themselves will have objects
              let xOffset = 0;
              let yOffset = 0;
              let x1 = dimensions[0].values(201);
              if (x1.length > 200) {
                alert("Only rendering max 200 records.");
              }
              x1.forEach (function (dd, i) {
                dd.type = "circle";
                dd.boundary = 'A';
                dd.x = (xOffset*50) + 300;
                dd.y = 60 + (30*yOffset);
                if (xOffset == 15) {
                    xOffset = 0;
                    yOffset++;
                }
                xOffset++;
                dd.fixed = true;
                dd.name = "I"+i;

                links.push({"source":"B", "target":dd.name, "type":"linear"})
              });

              newData.x = 110;
              newData.y = 60;
              newData2.x = 110;
              newData2.y = 150;
              let nodes = x1;
              nodes.push(newData);
              nodes.push(newData2);
              nodes.push({"type":"box","label":"root", "name":"B", "boundary":"A", "fixed":true, "x":300, "y":80});
              let config = {
                               "enable_zone_boundaries":false,
                               "enable_collision_avoidance":false,
                               "background_color": "white",
                               "paper_width": 1000,
                               "fixed_width": false,
                               "preferred_ratio": 0.5,
                               "force_collide": 30
                             };

              let zones = [{ "type" : "default", "rectangle": [10, 10, "viewWidth-20", "viewHeight-20"]}];
              vc.setState({"config":config, "nodes":nodes, "links":links, "zones":zones})
              vc.render();
              vc.refreshSize();
          });
//          d3.json(source, function(json) {
//              vc.setState(json);
//              vc.render();
//
//              //vc.trigger("tabClick", json.nodes[0].children[0]);
//              vc.refreshSize();
//          });
      }
  }

  render() {
    return (
        <div id="base" ref="root">
        </div>);
  }

  renderModals() {
    ReactDOM.render(

      <div>
         <div id="myModal" className="modal fade" role="dialog">
           <div className="modal-dialog">
             <div className="modal-content">
               <div className="modal-header">
                 <button type="button" className="close" data-dismiss="modal">&times;</button>
                 <h4 className="modal-title">Modal Header</h4>
               </div>
               <div className="modal-body">
                 <p>Some text in the modal.</p>
               </div>
               <div className="modal-footer">
                 <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
               </div>
             </div>
           </div>
         </div>
      </div>

      ,document.getElementById('modals')
    );
  }
}

export default TheWrapper;