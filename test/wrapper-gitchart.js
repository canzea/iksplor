import React from 'react';
import ReactDOM from 'react-dom';

import * as d3 from 'd3';
import Registry from "../src/Registry";
import {ViewerCanvas} from "../src/ViewerCanvas";

import crossfilter from 'crossfilter';
import Dimension from '../src/utils/Dimension';


import { GitChart as GitChart } from '../src/.';

class TheWrapper extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {

      var self = this;
      function getParameterByName(name, url) {
            if (!url) {
              url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
      }


      let source = getParameterByName('p');

      new GitChart().build('base', 'https://github.com/hashicorp/consul.git', '/gitcharts/data.txt');

  }

  render() {
    let self = this;
    return (
        <div>
            <div id="base"></div>
        </div>);
  }

}

export default TheWrapper;