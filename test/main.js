import 'styles/custom.scss';

import React from 'react';
import ReactBootstrap from 'react-bootstrap';

import { AppContainer } from 'react-hot-loader';
import ReactDOM from 'react-dom';

import {
  HashRouter as Router,
  Route,
  Switch,
  NavLink
} from 'react-router-dom';

import App from './app.js';
import Wrapper from './wrapper';
import WrapperProgressTree from './wrapper-progress-tree';
import WrapperGitChart from './wrapper-gitchart';

const routes = (
  <Router>
    <Switch>
        <Route exact path="/" component={App} />
        <Route exact path="/examples/:id" component={Wrapper} />
        <Route path="/progress" component={WrapperProgressTree} />
        <Route path="/gitchart" component={WrapperGitChart} />
    </Switch>
  </Router>
);

const outlet = document.getElementById('app')

const render = () => {
  ReactDOM.render(
    <AppContainer>
      {routes}
    </AppContainer>,
    outlet
  );
};

render();

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept(render);
}
