import React from 'react';
import ReactDOM from 'react-dom';

import * as d3 from 'd3';
import Registry from "../src/Registry";
import {ViewerCanvas} from "../src/ViewerCanvas";

import crossfilter from 'crossfilter';
import Dimension from '../src/utils/Dimension';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/pairs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';


class TheWrapper extends React.Component {
  constructor(props) {
    super(props);
  }

  resetClick (e, vc) {
    vc.zoomReset();
  }

  moveClick (e, vc) {
    //vc.findNode('gocd')['flash'] = !(vc.findNode('gocd')['flash']);
    //vc.findNode('gocd')['state'] = vc.findNode('gocd')['state'] == "Provisioned" ? "warning":"Provisioned";

    vc.zoomBy ("scale(1) translate(200,100)");
    //vc.update();
  }

  componentWillMount() {
  }
  componentDidMount() {
      const { match: { params } } = this.props;

      console.log("Params = "+JSON.stringify(params));

      function getParameterByName(name, url) {
            if (!url) {
              url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
      }

      this.renderModals();

      let source = (params && params.id ? "examples/" + params.id + ".json" : ""); //getParameterByName('p');
      console.log("SOURCE = "+source);
      let self = this;

      let vc = new ViewerCanvas();
      vc.mountNode (self.refs.root);

      this.vc = vc;
      vc.on("tabClick", function (e, data, doc) {
          console.log("TAB CLICKED: " + JSON.stringify(data));
      });

      vc.on("shapeClick", function (e, data, doc) {
          console.log("SHAPE CLICK: " + JSON.stringify(doc));
          //data.selected = !data.selected;
          //doc.nodes[0].label = doc.nodes[0].label + 'x';
          //doc.nodes[0].values[0].selected = !data.selected;
          //doc.nodes[0].index = null;
          //doc.nodes.splice(i, 1);
          //let n = doc.nodes.pop();
          //vc.update();

          data.selected = !data.selected;
          //doc.nodes.push(n);

          vc.update();

          //console.log("SHAPE CLICKED: " + data.content.url);
          //window.location.href = data.content.url;
      });

      vc.on("shapeNavClick", function (e, data, doc) {
          console.log("SHAPE NAV CLICKED: " + JSON.stringify(data));
          $("#myModal").modal();
          vc.trigger("closeNavigation");
      });

      let vb="";
      vc.on("change", function (e, data, doc) {
          if (typeof(Storage) !== "undefined") {
             localStorage.setItem(source, JSON.stringify(doc));
          }
          vc.zoomToNode(0, 1);
          /*
          if (vb == "") {
              vb = vc.zoom("300 0 100 200");
          } else {
              vc.zoom(vb);
              vb = "";
          }
          */
      })

      window.onresize = function(event) {
          vc.refreshSize();
          vc.render();
      };

        d3.json(source, function(json) {
/*
            let view =
              {
                "config": {
                  "enable_zone_boundaries":true,
                  "enable_collision_avoidance":true,
                  "background_color": "white",
                  "paper_width": 800,
                  "preferred_ratio": 0.666,
                  "fixed_width": false,
                  "force_collide": 20,
                  "pan_and_zoom":true
                },
                "nodes": [],
                "links": [],
                "zones": [
                    { "type" : "default", "rectangle": [10, 10, "viewWidth-20", "viewHeight-20"]}
                ]
              };

            let x = 0;
            let nodes = [];
            let optimalWidth = Math.round((800-10)/Object.keys(json.environments).length);
            console.log("Optimal width = "+optimalWidth);
            let index = 0;
            let boundary = ['A','B','C','D','E','F','G','H','I','J','K','L'];

            let insts = [];
            let zoneLabels = [];

            Observable.from(json.environments).subscribe(function (envItem) {
                  let env = {
                      "type" : "nested",
                      "scale" : 1,
                      "icon" : "gear",
                      "name": envItem.name,
                      "label": envItem.label,
                      "boundary": "A",
                      "fixed": true,
                      "x": x,
                      "y": 50,
                      "inside": [
                      ]
                  }
                  view.zones.push(
                      { "orientation":"horizontal", "type" : "default2", "rectangle": [5 + x, 27, optimalWidth-10, 300]}
                  )
                  zoneLabels.push(
                      { "orientation":"horizontal", "type" : "default2", "rectangle": [5 + x, 5, optimalWidth-10, 20], "label":envItem.label}
                  )

                  let y = 45;
                  Observable.from(envItem.instances).subscribe (val => {
                      insts.push(
                          {"fixed":true, "class":"text-lg", "y": y, "x":x + 70, "state":val.status, "name": val.name, "flash": false, "type": "box", "position":"top", "label":val.name, "boundary":boundary[index], "scale":0.3 },
                      );
                      y+=20;
                  });

                  y = 45;
                  //let x = -50;
                  Observable.from(envItem.segments).subscribe (val => {
                      insts.push(
                          {"fixed":true, "class":"text-lg", "y": y, "x":x + -10, "state":val.status, "name": val.code, "flash": false, "type": "box", "position":"top", "label":val.label, "boundary":boundary[index], "scale":0.3 },
                      );
                      y+=20;
                  });

                  const source = Observable.from(json.serviceEndPoints);
                  const out = source.filter(endpoint => endpoint.environment == envItem.name);
                  out.subscribe(val =>
                      view.nodes.push(
                          {"state":val.instanceStateType, "name": val.name, "icon":"gear", "flash": false, "type": "component", "position":"top", "label":val.name, "boundary":boundary[index], "scale":0.3 },
                      )
                  );
                  //nodes.push(env);

                  x += optimalWidth;
                  index++;
            });


            view.zones = view.zones.concat(zoneLabels);
            view.nodes = view.nodes.concat(insts);

//              view.links.push({
//                  "source": "cd",
//                  "target": "play"
//              })

            //view.nodes = nodes;
*/

            vc.setState(json);
            vc.render();

            //vc.trigger("tabClick", json.nodes[0].children[0]);
            vc.refreshSize();
            vc.render();
        });

  }

  render() {
    let self = this;
    return (
        <div>
            <button ref="reset" onClick={(e) => this.resetClick(e, self.vc)}>Reset</button>
            <button ref="move" onClick={(e) => this.moveClick(e, self.vc)}>Move</button>
            <div id="base" ref="root"></div>
        </div>);
  }

  renderModals() {
    ReactDOM.render(

      <div>
         <div id="myModal" className="modal fade" role="dialog">
           <div className="modal-dialog">
             <div className="modal-content">
               <div className="modal-header">
                 <button type="button" className="close" data-dismiss="modal">&times;</button>
                 <h4 className="modal-title">Modal Header</h4>
               </div>
               <div className="modal-body">
                 <p>Some text in the modal.</p>
               </div>
               <div className="modal-footer">
                 <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
               </div>
             </div>
           </div>
         </div>
      </div>

      ,document.getElementById('modals')
    );
  }
}

export default TheWrapper;