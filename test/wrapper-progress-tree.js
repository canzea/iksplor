import React from 'react';
import ReactDOM from 'react-dom';

import * as d3 from 'd3';
import Registry from "../src/Registry";
import {ViewerCanvas} from "../src/ViewerCanvas";

import crossfilter from 'crossfilter';
import Dimension from '../src/utils/Dimension';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/pairs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/count';
import 'rxjs/add/operator/groupBy';
import 'rxjs/add/operator/filter';

import { ProgressTree as ProgressTree } from '../src/.';

class TheWrapper extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {

      var self = this;
      function getParameterByName(name, url) {
            if (!url) {
              url = window.location.href;
            }
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
      }


      let source = getParameterByName('p');

//      new ProgressTree().buildFromFile('base', source);
      new ProgressTree().buildFromFile('base', 'examples/progress.csv', {timeRange:30,maxStreams:5,streamLabelWidth:0});
      new ProgressTree().buildFromFile('base', '/examples/progress_10.json');
      new ProgressTree().buildFromFile('base', 'examples/progress_70.json');
      new ProgressTree().buildFromFile('base', 'examples/progress_90.json');
      var pt = new ProgressTree();
//      pt.buildFromFile('base', 'examples/progress_100.json');
//      pt.update();
//      pt.addLines();

      var test = new ProgressTree()
/*
      d3.json('http://localhost:8888/api/proto/preview', (err, resources) => {

          var posMap = {}
          resources['resources'].map((job, i) => {
            posMap[job._id] = job
          });

          var stream = 0;
          var previousJob;
          resources['resources'].map((job, i) => {
             job.id = job._id;
             job.parallel = 1;
             job.position = (previousJob ? previousJob.position + 1 : 0);
             job.parent = '';
             if (job.hasOwnProperty('depends_on')) {
                job.parent = job['depends_on'][0];
                console.log("Looking for: " + job['depends_on'][0]);
                console.log("Setting position to " + posMap[job.parent].position);
                job.position = posMap[job.parent].position + 1;
                job._stream = "Stream-" + stream++;
             } else {
                job._stream = (previousJob ? previousJob._stream : 'default');
             }
             job.type = Object.keys(job)[0];

             previousJob = job;
          });

          var streams = []
          var records = resources['resources']
          var mapIndex = {}

          var groupBy = "_type";

          self.buildStreams(records, groupBy, streams)

          streams.map((s,i) => { mapIndex[s.key] = i });

          resources['resources'].map((job, i) => {
              job.parallel = mapIndex[job[groupBy]]
          });

          test.buildFromData('base', records, streams.map((s) => (s.key + " (" + s.count + ")")), {maxStreams:streams.length, height: (streams.length*15)+30, streamLabelWidth:200});
          test.update();
          test.addLines();
      })
      */

/*
      test.buildFromJSON("base", data);
            test.addLines();

          data.streams.push('naother');
          test.getData().push(test.toJSON(data.header, [3,2,3,'id1',"pending"]));
          test.getData().push(test.toJSON(data.header, [4,2,4,0,"pending"]));
          test.getData().push(test.toJSON(data.header, [5,3,5,0,"busy"]));
          test.update();
            test.replaceLines();

      setTimeout(() => {

          setTimeout(() => {
            console.log("BUSY");
            test.updateValue(5, {state:'busy'})
          }, 1000);
          setTimeout(() => {
            console.log("COMPLETE");
            test.getData().splice(0, test.getData().length);
            test.getData().push(test.toJSON(data.header, [6,3,8,0,"pending"]));
            test.getData().push(test.toJSON(data.header, [7,3,10,0,"pending"]));
            test.update();
            test.replaceLines();
          }, 2000);
          setTimeout(() => {
            console.log("NEXT");
            test.getData().splice(0, test.getData().length);
            test.getData().push(test.toJSON(data.header, ['id1',0,0,0,"complete"]));
            test.getData().push(test.toJSON(data.header, [3,2,3,'id1',"complete"]));
            test.getData().push(test.toJSON(data.header, [4,2,4,0,"busy"]));
            test.getData().push(test.toJSON(data.header, [5,3,5,0,"pending"]));
            test.getData().push(test.toJSON(data.header, [15,3,15,0,"pending"]));
            test.getData().push(test.toJSON(data.header, [25,3,25,0,"pending"]));
            test.getData().push(test.toJSON(data.header, [35,3,35,0,"pending"]));
            for ( var i= 0 ; i<  30; i++) {
                test.getData().push(test.toJSON(data.header, [45 + i,3,45 + i,0,"pending"]));
            }
            test.update();
            test.replaceLines();
          }, 4000);
          setTimeout(() => {
            console.log("NEXT");
            test.updateValue (2, {state:'complete'});
            test.updateValue (3, {state:'busy'});
            test.update();
          }, 6000);
      }, 1000);
      */
  }

  buildStreams(list, groupBy, streams) {

        var source = Observable
            .from(list)
            .groupBy ((f) => {
                return f[groupBy];
        });
        let output = []
        source.subscribe((obs) => {
            obs.count().subscribe(function (x) {
                output.push ({"key" : obs.key, "count" : x});
            });
        });

        output
            .sort(function (a,b) { return (a.key.toUpperCase() < b.key.toUpperCase() ? -1:1) })
            .map ( (row, index) => {
                streams.push(row);
            });
        return streams;
  }

  render() {
    let self = this;
    return (
        <div>
            <div id="base"></div>
        </div>);
  }

}

export default TheWrapper;