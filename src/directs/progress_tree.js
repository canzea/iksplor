
import * as d3 from 'd3';
import './progress_tree.scss';

class ProgressTree {

    constructor () {
        this.index = {};
        this.defaultOptions = {timeRange:30,maxStreams:5,streamLabelWidth:50, height:120};

        this.fillOptions = {busy:'orange',complete:'green',error:'red',pending:'#555555', RUNNING:'orange',COMPLETE:'green',FAILED:'red',PENDING:'#555555',SCHEDULED:'orange'}
        this.force = d3.forceSimulation();
    }

    buildFromFile(id, file, options = {}) {
        var self = this;
        if (file.endsWith('.json')) {
            d3.json(file + "?v=" + Math.random(), (err, data) => {

                self.buildFromJSON(id, data, options);
                self.addLines();
            });
        } else {
            d3.csv(file + "?v=" + Math.random(), (err, data) => {
                if (err) { raise(err); }

                self.buildFromData(id, data, [], options);
                self.addLines();
                //self.adjustHeight(self.streams.length * 20);
                //self.update();
            });
        }
    }

    getData() {
        return this.data;
    }

    updateValue (id, values) {
        Object.keys(values).map(
            (key) => { this.data[id][key] = values[key]; })
    }

    toJSON(header, data) {
        var row = {};
        header.map( (h,i) => { row[h] = data[i]; } )
        return row;
    }

    buildFromJSON(id, data, options = {}) {
        var newData = data['records'].map( (d) => {
            var row = {};
            data['header'].map( (h,i) => { row[h] = d[i]; } )
            return row;
        });
        this.buildFromData(id, newData, data.streams, options);
    }

    addLines() {
        var x = this.x;
        var y = this.y;
        var lineData = this.lineData;
        var graph = this.graph;
        var line = d3.line()
            .x(function(d) { return x(d.position); })
            .y(function(d) { return y(d.parallel); });

        graph.select('g.lines')
           .selectAll('path.progress-graph-edge')
           .data(lineData)
           .enter()
           .append("path")
           .attr("d", line)
           .attr("class", "progress-graph-edge")
           .style("opacity", 0)
           .transition()
           .ease(d3.easeLinear)
           .duration(200)
           .style("opacity", 1);
    }

    replaceLines() {
        var self = this;
        var graph = this.graph;
        graph.select('g.lines')
           .selectAll('path.progress-graph-edge')
           .style("opacity", 1)
           .transition()
           .ease(d3.easeLinear)
           .duration(200)
           .style("opacity", 0)
           .remove()
           .on('end', self.addLines.bind(self));
    }

    update() {
            var div = this.div;
            var index = this.index;
            var svg = this.svg.select('g');
            var data = this.data;
            var streams = this.streams;
            var x = this.x;
            var y = this.y;
            var options = this.options;
            var graph = this.graph;
            var width = this.width;

            var fillOptions = this.fillOptions;

            var maxParallel = d3.max(data.map((d) => Number(d.parallel)));
            var maxPosition = d3.max(data.map((d) => Number(d.position)));

            var r = maxPosition < 60 ? 4:(maxPosition < 80 ? 4:(maxPosition < 100 ? 3:3));

            maxParallel = Math.max(maxParallel, streams.length);

            x.domain([0, maxPosition]);
            y.domain([0, Math.max(options.maxStreams, streams.length)]);

            graph.selectAll("circle")
               .data(data, (i) => i.id)
               .exit()
               .style("opacity", 1)
               .transition()
               .ease(d3.easeLinear)
               .duration(400)
               .style("opacity", 0)
               .remove();

            var newNodes = graph.selectAll("circle")
               .data(data, (i) => i.id)
               .enter().append("circle");

            var hiddenNodes = [];

//            var splices = [];
//
////            data.filter((row) => row.parent == 'hidden').map((r,i) => { splices.push(i); });
//
//            splices.map((s) => {
//                data.splice (s, 1);
//            })
//
//            splices = [];

            if (newNodes.size() > 0) {
                // re-calculate the hidden nodes if there has been a change to the nodes
                this.lineData = [];
                let branches = {};
                branches['default'] = []

                this.lineData.push(branches['default']);

                let currentBranch = 'default';

                data.map((row,i) => {
                    index[row.id] = row;

                    if (row.parent != 0 && row.parent != "" && row.parent != 'hidden') {
                        // branch off a new line
                        branches[row.id] = []
                        this.lineData.push(branches[row.id]);
                        currentBranch = row.id;

                        // Go backwards to find the parent and add it to the new branch
                        var ind = i - 2;
                        while (ind >= 0) {
                            if (data[ind].id == row.parent) {
                                branches[currentBranch].push(data[ind])
                            }
                            ind--;
                        }
                    }
                    branches[currentBranch].push(row);
                });
            }
//            splices.map((s) => {
//                data.splice (s[0], s[1], s[2]);
//            })

            newNodes
                .attr("class", "progress-graph-node")
                .attr('fill', (d) => fillOptions[d.state])
                .attr("r", () => r)
                .attr("cx", (d) => x(d.position))
                .attr("cy", (d) => y(d.parallel))
             .attr("stroke-opacity", "0.0")
             .on("mouseover", function(d) {
               div.transition()
                 .duration(200)
                 .style("opacity", .9);
               div.html(d.state)
                 .style("left", (d3.event.pageX + 5) + "px")
                 .style("top", (d3.event.pageY - 25) + "px");
               })
             .on("mouseout", function(d) {
               div.transition()
                 .duration(500)
                 .style("opacity", 0);
               });

//                .filter((d) => (d.state != 'busy' && d.state != 'RUNNING'))
//                .style("opacity", 0)
//                .transition()
//                .ease(d3.easeLinear)
//                .duration(400)
//                .style("opacity", 1);



            if (options.timeRange != 0) {
                var axis = d3.scaleLinear().range([options.streamLabelWidth, width])
                axis.domain([0,options.timeRange]);

                svg.selectAll('g.tick-group').data([maxParallel], (d) => d).exit().remove();
                svg.selectAll('g.tick-group').data([maxParallel], (d) => d).enter().append("g")
                  .classed("tick-group", true)
                  .style('font-size', '0.5em')
                  .attr("transform", "translate(0," + (y(maxParallel)) + ")")
                  .attr("opacity", 0)
                  .transition()
                  .ease(d3.easeLinear)
                  .duration(800)
                  .attr("opacity", 1)
                  .call(d3.axisBottom(axis).tickFormat((e) => ''+e + 'm'));
            }

            svg
                .selectAll('text.progress-graph-stream')
                .data(streams, (s) => s)
                .exit()
                .remove();

            svg
                .selectAll('text.progress-graph-stream')
                .data(streams, (s) => s)
                .enter()
                .append("text")
                .attr("class", "progress-graph-stream")
                .attr("x", (d) => -5)
                .attr("y", (d,i) => y(i) + (r/2) )
                .text((d) => d)

            function ticked() {
                graph.selectAll('circle')
                .attr('fill', (d) => fillOptions[d.state])
            }


            this.force = d3.forceSimulation();
            this.force
                .nodes(newNodes)
                .on("tick", ticked);

            function repeat(node) {
                node
                    .filter((d) => { return d.state == 'busy' || d.state == 'RUNNING'; })
                    .transition()
                    .ease(d3.easeLinear)
                    .duration(800)
                    .attr("fill", "white")
                    .attr("stroke", "orange")
                    .attr("stroke-width", "0.1em")
                    .transition()
                    .ease(d3.easeLinear)
                    .duration(800)
                    .attr("fill", "orange")
                    .attr("stroke", "orange")
                    .attr("stroke-width", "0em")
                    .on('end', () => repeat(node));
            }

            repeat (newNodes); // graph.selectAll('circle')

    }

    adjustHeight () {
        var height = (this.streams.length*15) - 10;
        this.svg.attr("viewBox", "0 0 800 " + height);
        //this.y = d3.scaleLinear().range([5, height-30]);
    }

    buildFromData(id, data, streams = [], options = {}) {

        Object.keys(this.defaultOptions).map(
            (o) => { if ( !options.hasOwnProperty(o) ) { options[o] = this.defaultOptions[o] } })

        var margin = {top: 5, right: 15, bottom: 5, left: 15},
            width = 800 - margin.left - margin.right,
            height = Math.max(options.height,(streams.length*15)) - margin.top - margin.bottom;

        var div = d3.select("body").append("div")
            .attr("class", "iksplor-tooltip")
            .style("opacity", 0);

        var svg = d3.select("#" + id).append("svg")
          .attr("preserveAspectRatio", "xMinYMin")
          .attr("viewBox", "0 0 800 " + height)
          .classed("progress-tree", true);

        var frame = svg.append("g")
          .attr("transform",
                  "translate(" + margin.left + "," + margin.top + ")");

        var x = d3.scaleLinear().range([options.streamLabelWidth, width]);
        var y = d3.scaleLinear().range([5, height-30]);

        var map = {}

        var graph = frame.append("g");
        graph.append('g').classed('lines', true);


        graph.selectAll('circle')
            .filter((d) => { return d.parent == 'hidden'; })
            .attr('opacity', 0);

        this.data = data;
        this.streams = streams;
        this.x = x;
        this.y = y;
        this.options = options;
        this.graph = graph;
        this.width = width;
        this.svg = svg;
        this.div = div;

        this.update();
    }
}

export default ProgressTree;