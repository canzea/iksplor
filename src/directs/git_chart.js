import * as d3 from 'd3';

import './git_chart.scss'


class GitChart {

    constructor () {
    }

    build(id, reference, dataFile) {
        var margin = {top: 5, right: 15, bottom: 5, left: 15};

//        var svg = d3.select("#" + id).append("svg")
//          .attr("preserveAspectRatio", "xMinYMin")
//          .attr("viewBox", "0 0 800 " + "100")
//          .classed("progress-tree", true)
//          .append("g")
//          .attr("transform",
//                  "translate(" + margin.left + "," + margin.top + ")");



        this.graph_week (id, reference, dataFile);

    }

    graph_week(id, source, file, milestones = []) {
        // set the dimensions and margins of the graph
        var margin = {top: 20, right: 60, bottom: 40, left: 40},
            width = 800 - margin.left - margin.right,
            height = 200 - margin.top - margin.bottom;

        // parse the date / time
        var parseTime = d3.timeParse("%Y-%U");


        // set the ranges
        var x = d3.scaleTime().range([0, width]);
        var y = d3.scaleLinear().range([height, 0]);
        var y2 = d3.scaleLinear().range([height, 0]);

        // define the line
        var valueline = d3.area()
            .x(function(d) { return x(d.date); })
            .y1(function(d) { return y(d['#commits']); })
            .curve(d3.curveCardinal);

        var valueline2 = d3.line()
            .x(function(d) { return x(d.date); })
            .y(function(d) { return y2(d['#users']); })
            .curve(d3.curveCardinal);


        // append the svg obgect to the body of the page
        // appends a 'group' element to 'svg'
        // moves the 'group' element to the top left margin
        var wrap = d3.select("#" + id).append("div")
        wrap.attr("class", "git-chart");


        var div = d3.select("body").append("div")
            .attr("class", "iksplor-tooltip")
            .style("opacity", 0);

        var svg = wrap.append("svg")
           // .attr("transform", "scale(0.4) translate("+-(width/2)+","+-(height/2)+")")
           // .attr("width", width + margin.left + margin.right)
           // .attr("height", height + margin.top + margin.bottom)
            .attr("preserveAspectRatio", "xMinYMin")
            .attr("viewBox", "0 0 800 200")
          .append("g")
            .attr("transform",
                  "translate(" + margin.left + "," + margin.top + ")");

        // Get the data

      //data = data.filter((d) => d.year == '2017');
        d3.csv(file, function(error, data) {
          if (error) throw error;

          // format the data
          var cumu = 0;
          data.map(d => d.date = Number("" + d.year + d.week));

          data.sort((a,b) => d3.ascending(Number(a.year + a.week.padStart(2,'0')), Number(b.year + b.week.padStart(2,'0')))).forEach((d) => {
              //console.log("NUMBER = "+d.year + '-' + Number(''+d.year + d.date) + " : " + d.close + " : " + parseTime(d.year + '-' + d.date));
              d.date = parseTime(d.year + '-' + d.week);
              cumu += Number(d['#commits']);
              d['#commits'] = cumu;
          });

          var mindate = new Date(2017,1,1);
          var subset = data.filter((d) => d.date > mindate);

          var maxdate = new Date();//2019,12,31

          // Scale the range of the data
          x.domain([mindate, maxdate]); // d3.extent(data, function(d) { return d.date; })
          y.domain([0, d3.max(data, function(d) { return Number(d['#commits']); })]);
          y2.domain([0, d3.max(data, function(d) { return Number(d['#users']); })]);
          valueline.y0(y(0));

          // Add the valueline path.
          svg.append("path")
              .data([subset])
              .attr("class", "line-commits")
              .attr("d", valueline);

          svg.append("path")
              .data([subset])
              .attr("class", "line-users")
              .attr("d", valueline2);

          // add the dots with tooltips
          svg.selectAll("dot")
             .data(subset)
           .enter().append("circle")
             .attr("r", 5)
             .attr("cx", function(d) { return x(d.date); })
             .attr("cy", function(d) { return y2(d['#users']); })
             .attr("fill-opacity", "0.0")
             .on("mouseover", function(d) {
               div.transition()
                 .duration(200)
                 .style("opacity", .9);
               div.html(d.year + "-" + d.week + "<br/>" + " Commits " + d["#commits"] + " Users " + d["#users"])
                 .style("left", (d3.event.pageX + 5) + "px")
                 .style("top", (d3.event.pageY - 25) + "px");
               })
             .on("mouseout", function(d) {
               div.transition()
                 .duration(500)
                 .style("opacity", 0);
               });

          milestones.map ((m) => {
              milestone (svg, m, {"height":height, "x":x})
          });

          // Add the X Axis
          // https://github.com/d3/d3-time-format
          svg.append("g")
              .attr("transform", "translate(0," + height + ")")
              .call(d3.axisBottom(x).tickFormat(d3.timeFormat("%Y-%b")));

          // Add the Y Axis
          svg.append("g")
              .call(d3.axisLeft(y2));

          svg.append("g")
              .attr("transform", "translate(" + width + ",0)")
              .call(d3.axisRight(y));

          svg
              .append("text")
              .attr("text-anchor", "middle")
              .text("Cumulative Commits")
              .classed('axis-label', true)
              .attr("transform", "translate(" + (55 + width) + "," + (height / 2) + ") rotate(-90)");
          svg
              .append("text")
              .attr("text-anchor", "middle")
              .text("Contributors")
              .classed('axis-label', true)
              .attr("transform", "translate(" + (-30) + "," + (height / 2) + ") rotate(-90)");

          svg
              .append("text")
              .attr("text-anchor", "left")
              .text("Source: " + source)
              .classed('axis-label-source', true)
              .attr("transform", "translate(" + (-40) + "," + (height + 35) + ")");
        });
    }
}

export default GitChart;
