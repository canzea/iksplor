
import fa from 'fontawesome';
import * as d3 from "d3";
import './style.scss';
import {ViewerCanvas} from '../../ViewerCanvas';

class Layout {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(d3visual, zone, root, viewer) {
      let self = this;
      viewer.on("tabClick", function (event, e) {
          let tabsToOpen = [];
          let tabToOpen = e.name;
          while (tabToOpen != null ) {
              let tabNode = d3visual.selectAll("g.tab")
                .filter(function(d) {
                    return d.name == tabToOpen;
              });
              if (tabNode.classed("selected_tab") == false || e.name == tabNode.datum().name) {
                  tabsToOpen.push(tabNode);
              }
              tabToOpen = tabNode.datum().layout.parent;
          }
          let to = tabsToOpen.reverse();
          for (let ind in to) {
              let clickedObject = to[ind];
              self.toggleTab(viewer, clickedObject.datum(), clickedObject.node());
          }
      });
  }

  enrichData (nodes) {
//
//      let segs = [];
//      for ( let x = 0 ; x < graph.nodes.length; x++) {
//          let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
//          if (seg != "" && segs.indexOf(seg) < 0) {
//              segs.push(seg);
//          }
//      }
      let badNodes = [];
      let fullNodeList = [];

      this.recurse(null, nodes, "", 0, badNodes, fullNodeList);

//      // calculate the segment layout
//      //var segs = ["JavaStack", "ReactStack","Configuration", "App","Database","Monitoring","Config","Security","Deploy","Runtime","Infrastructure","Develop","Registry"]
//      for ( let p = 0; p < segs.length; p++) {
//          let tot = 0;
//          let x,num;
//          for ( x = 0 ; x < graph.nodes.length; x++) {
//              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
//              if (seg == segs[p]) {
//                  tot++;
//              }
//              if (graph.nodes[x].children) {
//                  let ls = graph.nodes[x].children;
//                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
//                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
//                      if (seg == segs[p]) {
//                        tot++;
//                      }
//
//                      if (ls[xL1].children) {
//                          let lsL2 = ls[xL1].children;
//                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
//                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
//                              if (seg == segs[p]) {
//                                tot++;
//                              }
//                          }
//                      }
//                  }
//              }
//          }
//          for ( num = 0,x = 0 ; x < graph.nodes.length; x++) {
////              this.recurse(graph.nodes, 1);
//              let seg = (graph.nodes[x].title && graph.nodes[x].title.segment ? graph.nodes[x].title.segment : "");
////              if (seg == segs[p]) {
////                  graph.nodes[x].layout = { "seq":num,"total":tot,"seg":segs[p], "segnum":p, "level":0 };
////                  num++;
////              }
////              this.recurse(graph.nodes[x], 1);
////              if (graph.nodes[x].children) {
////                  let ls = graph.nodes[x].children;
////                  for ( let numL1 = 0,xL1 = 0 ; xL1 < ls.length; xL1++) {
////                      let seg = (ls[xL1].title && ls[xL1].title.segment ? ls[xL1].title.segment : "");
////                      if (seg == segs[p]) {
////                          ls[xL1].layout = { "seq":numL1,"total":tot,"seg":segs[p], "segnum":p, "level":1 };
////                          numL1++;
////                      }
////                      this.recurse(ls[xL1], 2);
//
////                      if (ls[xL1].children) {
////                          let lsL2 = ls[xL1].children;
////                          for ( let numL2 = 0,xL2 = 0 ; xL2 < lsL2.length; xL2++) {
////                              let seg = (lsL2[xL2].title && lsL2[xL2].title.segment ? lsL2[xL2].title.segment : "");
////                              if (seg == segs[p]) {
////                                  lsL2[xL2].layout = { "seq":numL2,"total":tot,"seg":segs[p], "segnum":p, "level":2 };
////                                  numL2++;
////                              }
////                          }
////                      }
//
//           //       }
//            //  }
//          }
//      }
  }

  recurse (parent, nodes, rootTag, level, badNodes, fullNodeList) {
      let tot = 0;
      for ( let num = 0,x = 0 ; x < nodes.length; x++) {
          const nd = nodes[x];
          if (nd.type == "tab") {
            tot++;
          }
      }
      for ( let num = 0,x = 0 ; x < nodes.length; x++) {
          const nd = nodes[x];
          if (nd.type == "tab") {
              nd.layout = { "seq":num,"total":tot,"seg": rootTag, "segnum":level, "level":level, "parent": (parent == null ? null:parent.name) };
              num++;

              if (nd.hasOwnProperty("name") == false) {
                alert("Warning: Tab nodes require name " + JSON.stringify(nd));
                badNodes.push(nd);
              } else if (nd.name in fullNodeList) {
                alert("Warning: Tab node names must be unique " + JSON.stringify(nd));
                badNodes.push(nd);
              } else {
                fullNodeList[nd.name] = nd;
              }

              if (nd.children) {
                  this.recurse(nd, nd.children, rootTag + "_L" + (level) + "T" + (num-1), level+1, badNodes, fullNodeList);
              }
          }
      }
  }

  configEvents(d3visual, newNodesTotal, zones, viewer) {
      let self = this;

      let newNodes = newNodesTotal.filter(function(d) { return d.type == 'tab'; });

      let force = viewer.force;

      // Use x and y if it was provided, otherwise stick it in the center of the zone
      newNodes
          .attr("cx", function(d) { const dim = self.getDim(d, zones); return d.x = (d.x ? d.x : (dim.x1 + (dim.x2-dim.x1)/2)); })
          .attr("cy", function(d) { const dim = self.getDim(d, zones); return d.y = (d.y ? d.y : (dim.y1 + (dim.y2-dim.y1)/2)); });

//nodeSet, index
      newNodes
        .on('mouseenter', function (e) {
//            $(this).addClass("node-hover");
//            console.log("NODE: " +JSON.stringify(nodeSet));
        })
        .on('mouseleave', function (e) {
            //$(this).removeClass("node-hover");
        })
        .on('click', function (e) { viewer.trigger("tabClick", e); /*self.toggleTab(viewer, e, this);*/ /*self.toggleNavigation(viewer, e, this); */});

        // NO DRAG/DROP support for tabs
        function dragstarted(d) {
            viewer.downX = Math.round(d.x + d.y);
        }
        newNodes.call(d3.drag()
            .on("start", dragstarted));

        self.attachInfoController(viewer, newNodes);
  }

  attachInfoController(viewer, chgSet) {
      const self = this;

      chgSet.filter(function (d) { return d.hasOwnProperty("notes"); }).append("text")
          .attr("text-anchor", "middle")
          .text(function(d) { return fa('info-circle'); })
          .classed("decoration", true)
          .classed("option_x", true)
          .classed("icon", true)
          .on("click", function(e) { self.toggleNavigation(viewer, e, this.parentNode); });
  }


  getDim(d, zones) {
      const index = d.boundary.charCodeAt(0) - 65;
      if (zones.length <= index) {
          //console.log("INVALID BOUNDARY: " + d.boundary);
      }
      const rect = (zones[index].hasOwnProperty('calculatedRectangle') ? zones[index].calculatedRectangle : zones[index].rectangle);
      const dim = {x1:rect[0],y1:rect[1],x2:rect[0] + rect[2],y2:rect[1] + rect[3]};
      //console.log("K: " + JSON.stringify(dim, null, 2));
      return dim;
  }

  organize(d3visual, zoneIndex, zones) {

       let vb = d3visual.attr("viewBox").split(' ');

       let viewWidth = vb[2];

       let maxWidth = viewWidth - 0;

       d3visual.select("g.nodeSet").selectAll("g.node, g.nodec").filter(function (d) {
           const index = d.boundary.charCodeAt(0) - 65;
           return (index == zoneIndex);
       })
       .each(function (d) {
           d.x = (((maxWidth/d.layout.total) - 5)/2) + (d.layout.seq * (maxWidth/d.layout.total));
           d.y = (50/2) + (d.layout.segnum * 55);
          d3.select(this).select("rect")
            .attr("x", function (d) { return -((maxWidth/d.layout.total) - 5)/2; } )
            .attr("y", function (d) { return -d3.select(this).attr("height") / 2; } )
            .attr("width", function (d) { return (maxWidth/d.layout.total); } )

          d3.select(this).select("text.option_x").attr("transform", function(a) {
            return "translate(" + (((maxWidth/d.layout.total)/2)-15) + ", -10)";
          });

          d3.select(this).select("text").attr("transform", function(a) {
            const bbox = this.getBBox();
            const pbbox = d3.select(this.parentNode).select("rect").node().getBBox();
            let scale = 1;
            if ((pbbox.height / bbox.height) < pbbox.width / (bbox.width + 20) && (pbbox.height / bbox.height) < 1) {
                scale = (pbbox.height / bbox.height) == 0 ? 1:(pbbox.height / bbox.height);
            } else {
                scale = Math.min(pbbox.width / (bbox.width + 20), 1) == 0 ? 1:Math.min(pbbox.width / (bbox.width + 20), 1);
            }
            return "scale(" + scale + ")";

          });
       });

        d3visual.select("g.nodeSet").selectAll("g.tab").filter(function (d) {
           return (d.layout.level != 0);
        }).style("opacity", 0).style("display","none");

/*
       //maxWidth = 800;
       d3visual.select("g.nodeSet").selectAll("g.nodec").filter(function (d) {
           const index = d.boundary.charCodeAt(0) - 65;
           return (index == zoneIndex);
       })
       .attr("transform", function(d) {
            let offsetX = d.layout.seq;
            return "translate(" +(((maxWidth/d.layout.total)/2)+(offsetX * (maxWidth/d.layout.total))) + ", " + (50+(d.layout.level * 52)) + ")"; } )
       .each(function (d) {
           d.x = ((maxWidth/d.layout.total)/2) + (d.layout.seq * (maxWidth/d.layout.total));
           d.y = 50 + (d.layout.segnum * 55);

//           if (!d.layout) {
//                d.layout = {"segnum":2, "total":3};
//           }
           //d.x = 105 + (self.aa[d.layout.segnum] * (maxWidth/d.layout.total));
           //d.y = 70 + (d.layout.segnum * 75);
//           self.aa[d.layout.segnum] = self.aa[d.layout.segnum] + 1;
//           d3.select(this).attr("transform", "translate(" + d.x + "," + d.y + ")");
          d3.select(this).select("rect")
            .attr("x", function (d) { return -((maxWidth/d.layout.total) - 5)/2; } )
            .attr("y", function (d) { return -d3.select(this).attr("height") / 2; } )
            .attr("width", function (d) { return (maxWidth/d.layout.total) - 5; } );

          d3.select(this).select("text.option_x").attr("transform", function(a) {
            return "translate(" + (((maxWidth/d.layout.total)/2)-15) + ", -10)";
          });

          d3.select(this).select("text").attr("transform", function(a) {
            const bbox = this.getBBox(); const pbbox = d3.select(this.parentNode).select("rect").node().getBBox();
            return "scale(" + Math.min(pbbox.width / (bbox.width + 20), 1) + ")";
          });
          //d3.select(this).select("rect").attr("width", (self.width - 5));
       });
       */
  }


  toggleNavigation (viewer, e, object) {
     d3.event.stopPropagation();
     if (viewer.downX == Math.round(e.x + e.y)) {
        viewer.navigation.toggle(e, d3.select(object));
     } else {
        viewer.navigation.close(e);
     }
  }

  toggleTab (viewer, d, currentTab) {
     let self = this;

     //d3.event.stopPropagation();

     // get the child tabs
     let children = d3.select(currentTab.parentNode).selectAll("g.node_"+d.name);


     if (children.size() > 0) {

         let toggleOn = children.style("display") == "none";

         this.turnOffPreviousTab(d, currentTab);

//         if (toggleOn) {
             children.style("opacity", 0).transition().duration(1000).style("opacity", 1).style("display","block");
             d3.select(currentTab).classed("selected_tab", true);
             //d3.select(currentTab.parentNode).select("text.option_x").text(function (d) { return "-"; });
//         } else {
             //children.transition().duration(1000).style("opacity", 0).style("display","none"); //.each("end", function (e) { d3.select(this).style("display","none")
             //d3.select(currentTab).classed("selected_tab", false);
             //d3.select(currentTab.parentNode).select("text.option_x").text(function (d) { return "+"; });
//         }
     } else {

        // if there are no child tabs, look for other content, such as another svg/screenshot/etc
        this.turnOffPreviousTab(d, currentTab);

        if (d3.select(".page").size() == 0) {
            this.turnOffPreviousTab(d, currentTab);
            d3.select(currentTab).classed("selected_tab", true);

            if (!d.content) {
                let content = d3.select(viewer.domNode.parentNode).append("div").classed("page", true);
                content.style("top", "0px");
                content.html("no content");
                return;
            }

            let url = (d.content.url.indexOf('?') == -1 ? d.content.url + "?r=" + Math.random() : d.content.url + "&r=" + Math.random());
            console.log("Opening: " +url);
            d3.json(url, function(json) {


                let content = d3.select(viewer.domNode.parentNode).append("div").classed("page", true);

                //content.classed("level_" + d.layout.level, "true");

                //content.style("overflow", "hidden");

                self.refreshSize (viewer);

                let vc = new ViewerCanvas();
                vc.on("change", function(event, _irrelevant_data, doc) { viewer.trigger(event, d, doc); });
                vc.mountNode (content.node());
                vc.setState(json);
                vc.render();
                vc.refreshSize();

                self.vc = vc;
            });
        }
     }
  }

  refreshSize (viewer) {
    let vb = d3.select(viewer.domNode).attr("viewBox").split(' ');

    let tab = d3.select(viewer.domNode).select('g.nodeSet').node().getBoundingClientRect();
    let bodyRect = document.body.getBoundingClientRect();

    let viewWidth = vb[2];
    let viewHeight = vb[3];

    let pageWidth = d3.select(viewer.domNode).attr("width");
    let pageHeight = d3.select(viewer.domNode).attr("height");

    pageWidth = document.body.clientWidth;
    pageHeight = document.body.clientHeight;

    let top = Math.ceil((tab.top - bodyRect.top) + tab.height + 2);

    let full = {"height":pageHeight, "width":pageWidth};

    let content = d3.select(viewer.domNode.parentNode).select('.page');

    content.style("top", top + "px");
    content.style("left", "0px");
    content.style("width", (full.width) + "px");
    content.style("height", (full.height  - (top)) + "px");

    if (this.vc) {
        this.vc.refreshSize();
    }
  }

  turnOffPreviousTab(tabData, currentTab) {
     // turn off any previously selected tab
     d3.select(currentTab.parentNode).selectAll("g.selected_tab").each(function (p) {
         // For all tabs that are selected at new tab level or higher (lower in position)
         if (tabData.layout.level <= p.layout.level) {
             d3.select(this).classed("selected_tab", false);
             let selectedChildren = d3.select(currentTab.parentNode).selectAll("g.node_"+p.name);
             selectedChildren.transition().style("opacity", 0).style("display","none");
         }
     });
     //d3.select(currentTab).classed("selected_tab", false);

//     let self = this;
//     if (typeof self.vc != "undefined" && self.vc != null) {
//        console.log("destroy");
//        self.vc.destroy();
//        self.vc = null;
//     }
//
     //d3.select("#base").selectAll(".page").selectAll().remove();
     d3.select(currentTab.parentNode.parentNode.parentNode).selectAll(".page").remove();

  }
}

export default Layout;