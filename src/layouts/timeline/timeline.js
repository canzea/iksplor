
import * as d3 from "d3";
import './style.scss';

import moment from 'moment';
import Default from '../default.js';

class Layout extends Default {
  constructor(domainType) {
    super(domainType);
    this.domainType = domainType;
  }

  organize(d3visual, zoneIndex, zones) {
      let zone = zones[zoneIndex];

      let base = new Date(zone.start + '-01');
      let offset = base.getDay();

      //  moment().diff(date_time, 'minutes')
      d3visual.select("g.nodeSet").selectAll("g.node, g.nodec").filter(function (d) {
         const index = d.boundary.charCodeAt(0) - 65;
         return (index == zoneIndex);
      })
      .each(function (d, i) {
         if (zone.unit == "monthly" && d.hasOwnProperty("startDate")) {
            let days = moment(base).add(Number(offset) + Number(d.startDate), 'days').diff(base, 'days');

            d.x = 100 + (days * 22);
            d3.select(this).select("rect")
                .attr("width", function (d) { return (d.hasOwnProperty("duration") ? d.duration * 22 : 100); } )
         }
         if (zone.unit == "quarterly" && d.hasOwnProperty("startDate")) {
            let months = moment(base).add(Number(d.startDate), 'months').diff(base, 'months');

            d.x = 100 + (months * 30);

            d3.select(this).select("rect")
                .attr("width", function (d) { return (d.hasOwnProperty("duration") ? d.duration * 30 : 100); } )
            d3.select(this).select("circle")
                .attr("transform", function (d) { return "translate(" + (d.hasOwnProperty("duration") ? (d.duration * 30) + 6 : 100) + "," + 3 + ")"; });
         }
      });
  }

  build(d3visual, zone, root) {

      function addMonths(dateObj, num) {

        var currentMonth = dateObj.getMonth();
        dateObj.setMonth(dateObj.getMonth() + num)

        if (dateObj.getMonth() != ((currentMonth + num) % 12)){
            dateObj.setDate(0);
        }
        return dateObj;
      }


      const self = this;
      let obj = root.append("g");

      obj.classed("_zone", true);
      obj.classed("_" + this.domainType, true);

//      let vb = d3visual.attr("viewBox").split(' ');
//
//      let viewWidth = vb[2];
//      let viewHeight = vb[3];
//
//      let pageHeight = d3visual.node().parentNode.clientHeight;
//      let pageWidth = d3visual.node().parentNode.clientWidth;
//      // THis needs to be a calculation based on a ratio
//
//      let pageHeightSet = (typeof zone.rectangle[3] == "string" && zone.rectangle[3].indexOf("pageHeight") != -1);
//
//      for ( let v = 0; v < 4; v++) {
//          if (typeof zone.rectangle[v] == "string") {
//            //console.log("Evaluating: " + zone.rectangle[v]);
//            //console.log(" ... to : " + eval(zone.rectangle[v]));
//            zone.rectangle[v] = eval(zone.rectangle[v]);
//          }
//      }
//
//      if (pageHeightSet) {
//          zone.rectangle[3] = ((zone.rectangle[2]+zone.rectangle[0]) * pageHeight/pageWidth) - 20;
//      }

      this.calculateRectangle (d3visual, zone);

      let rect = obj.append("rect")
          .attr("class", "background")
          .attr("x", zone.calculatedRectangle[0])
          .attr("y", zone.calculatedRectangle[1])
          .attr("rx", 8)
          .attr("ry", 8)
          .attr("width", zone.calculatedRectangle[2])
          .attr("height", zone.calculatedRectangle[3]);//fff899


      // 26 weeks
      // 90*4 = 360

      // zone.unit == quarterly : show the month lines
      // zone.unit == monthly : show the weekly lines (with weekend)

      let milestoneGap = 0;
      let milestones = [];
      let bars = [];
      let level1 = [];
      if (zone.unit == "monthly") {
        const days = ["sun","mon","tue","wed","thu","fri","sat"];
        var date = new Date(zone.start + '-01');

        let segmentWidth = 22;
        milestoneGap = 210;

        let offset = date.getDay();
        if (offset >= 0 && offset <= 6) {
            date.setDate(date.getDate() - offset);
            for (let i = 0; i <= offset; i++) {
                milestones.push({"label":"" + date.getDate() + "", "position":100+(i*segmentWidth)});
                bars.push({"width":segmentWidth, "class":days[date.getDay()], "position":100 + (bars.length*segmentWidth) - (segmentWidth/2) });
                date.setDate(date.getDate() + 1);
            }
        }
        for (let i = (offset+1); i < 40; i++) {
            milestones.push({"label":"" + date.getDate() + "", "position":100+(i*segmentWidth)});
            bars.push({"width":segmentWidth, "class":days[date.getDay()], "position":100 + (bars.length*segmentWidth) - (segmentWidth/2) });
            date.setDate(date.getDate() + 1);
            if (date.getDate() == 1) {
                break;
            }
        }
        if ((offset) > 0) {
            let title = moment(date).subtract(1, 'month').format('MMMM YYYY');
            level1.push({"width":(segmentWidth*(offset+1)), "label": title, "class":"level1", "position":100 - (segmentWidth/2) });
        }
        let title = moment(date).format('MMMM YYYY');

        level1.push({"width":(segmentWidth*(bars.length-offset-1)), "label": title, "class":"level1", "position":100 + ((offset+1)*segmentWidth) - (segmentWidth/2) });

        zone.bars = bars;

      } else if (zone.unit == "quarterly") {

        const quarter = ["q1","q2","q3","q4"];
        const months = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"];
        date = new Date(zone.start + '-20');

        let segments = 3*9; // 10 quarters

        milestoneGap = segments * 30;

        for (let i = 0; i < segments; i++) {
            milestones.push({"label":"" + months[date.getMonth()] + "", "position":120+((i*30)+15)});
            if (date.getMonth()%3 == 0) {
                bars.push({"width":90, "class":quarter[Math.floor(date.getMonth()/3)], "position":120 + (bars.length*90) });
                if (date.getMonth()%12 == 0) {
                    if ((segments - i) < 12) {
                        level1.push({"width":(30*(segments-i))-5, "label": date.getFullYear(), "class":"level1", "position":120 + ((bars.length-1)*90) });
                    } else {
                        level1.push({"width":(30*3*4)-5, "label": date.getFullYear(), "class":"level1", "position":120 + ((bars.length-1)*90) });
                    }
                }
            }
            addMonths(date, 1);
        }

        zone.bars = bars;

      }

      obj.selectAll("text").data(milestones).enter().append("text")
          .attr("text-anchor", "middle")
          .attr("transform", function (d,i) { return "translate(" + (d.position) + ",40)"; })
          .text(function(d) { return d.label; });

      let count = 0;
      obj.selectAll("line").data(zone.milestones).enter().append("line")
          .attr("x1", function (d,i) { return 120 + (milestoneGap * i); })
          .attr("x2", function (d,i) { return 120 + (milestoneGap * i); })
          .attr("y1", zone.calculatedRectangle[1])
          .attr("y2", zone.calculatedRectangle[3])
          .attr("stroke", "gray")
          .attr("stroke-width", "1");

      const banner = obj.append("g").selectAll("rect").data(level1).enter();

      banner
        .append("rect")
        .attr("rx", 8)
        .attr("width", function (d) { return d.width; })
        .attr("height", 18)
        .attr("class", function (d) { return d.class; })
        .attr("y", 12)
        .attr("x", function (d, i) { return d.position; })

      banner
        .append("text")
        .attr("text-anchor", "middle")
        .classed("banner", true)
        .attr("transform", function (d,i) { return "translate(" + ((d.width/2) + (d.position)) + ",25)"; })
        .text(function(d) { return d.label; });


      obj.append("g").selectAll("rect").data(bars).enter().append("rect")
        .attr("width", function (d) { return d.width; })
        .attr("height", zone.calculatedRectangle[3] - 40)
        .attr("class", function (d) { return d.class; })
        .attr("y", zone.calculatedRectangle[1] + 40)
        .attr("x", function (d, i) { return d.position; });
  }
}

export default Layout;