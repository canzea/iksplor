
import * as d3 from 'd3';
import './style.scss';
import fa from "fontawesome";

import TextUtils from '../../utils/TextUtils';

class Shape {

  constructor(domainType) {
    this.domainType = domainType;
  }

  update(fullSet) {
      fullSet.each(function (d) {

          let valueNodes = d3.select(this).selectAll("g.value");

          valueNodes.select("rect")
              .attr("class", function (i) { return i.selected ? "selected":(i.noSelections ? "noselection":"unselected"); })

          valueNodes.select("text.item")
                    .text(function(i) { return i.label; });

          valueNodes.select("text.count")
                    .text(function(i) { return i.count; });

          valueNodes.select("text.icon")
                    .text(function(i) { return i.selected ? fa("check-square"):fa("square-o"); });
      })

  }

  build(chgSet) {
      var self = this;

      let x = 180;
      let y = 20;
      let rowHeight = 18;

      chgSet = chgSet.append("g").attr("transform", "scale(1.0)");

      chgSet.append("rect")
          .attr("x", -x/2)
          .attr("y", function (d) { return -(y + (d.values.length * rowHeight))/2; })
          .attr("width", x)
          .attr("height", function (d) { return y + (d.values.length * rowHeight); })

      chgSet.append("text")
          .attr("class", "label")
          .attr("text-anchor", "left")
          .attr("dx", 2 + (-x/2))
          .attr("dy", function (d) { return 8 - (y + (d.values.length * rowHeight))/2; })
          .text(function(d) { return d.label.toUpperCase(); });

      this.buildTools (chgSet, x, y);

      chgSet.each(function (d) {

          let valueNodes = d3.select(this).selectAll("g.value").data(d.values);
          let newNodes = valueNodes.enter()
            .append("g").classed("value", true);
          let pos = -5;

          newNodes.append("rect")
              .attr("x", -(x/2) + 0)
              .attr("y", function (i) { pos += rowHeight; return (7-((y + (d.values.length * rowHeight))/2) + pos); })
              .attr("width", x - 0)
              .attr("height", rowHeight)
              .attr("class", function (i) { return i.selected ? "selected":(i.noSelections ? "noselection":"unselected"); })

          pos = -5;
          newNodes.append("text")
                    .attr("class", "item")
                    .attr("text-anchor", "left")
                    .attr("dx", -(x/2) + 10 + 15)
                    .attr("dy", function (i) { pos += rowHeight; return (20-((y + (d.values.length * rowHeight))/2) + pos); })
                    .text(function(i) { return i.label; });
          pos = -5;
          newNodes.append("text")
                    .attr("class", "count")
                    .attr("text-anchor", "right")
                    .attr("dx", (x/2) - 20)
                    .attr("dy", function (i) { pos += rowHeight; return (18-((y + (d.values.length * rowHeight))/2) + pos); })
                    .text(function(i) { return i.count; });
          pos = -5;
          let xx = newNodes.append("text")
                    .attr("class", "icon")
                    .attr("text-anchor", "left")
                    .attr("dx", -(x/2) + 10)
                    .attr("dy", function (i) { pos += rowHeight; return (20-((y + (d.values.length * rowHeight))/2) + pos); })
                    .text(function(i) { pos+=rowHeight; return i.selected ? fa("check-square"):fa("square-o"); });

          newNodes.classed("clickable", true);
      })
  }

  buildTools (chgSet, x, y) {

      const rowHeight = 18;

      chgSet.append("rect")
          .classed('tools', true)
          .attr("x", -x/2)
          .attr("y", function (d) { return 11 -(y + (d.values.length * rowHeight))/2; })
          .attr("width", x)
          .attr("height", function (d) { return -11 + y + (d.values.length * rowHeight); })

      // apply paging... dots for 5 pages, then >> and << for paging, and a number box
      // apply search... by putting in text, will search for the options related to that text

      chgSet.append("text")
            .classed('tools', true)
            .classed('icon', true)
            .attr("text-anchor", "left")
            .attr("dx", -(x/2) + 4)
            .attr("dy", function (d) { return (18-((y + (d.values.length * rowHeight))/2)); })
            .text(function(i) { return fa('search'); });

      chgSet.append("text")
            .classed('tools', true)
            .classed('icon', true)
            .attr("text-anchor", "right")
            .attr("dx", (x/2) - 20 - 30)
            .attr("dy", function (d) { return (18-((y + (d.values.length * rowHeight))/2)); })
            .text(function(i) { return fa('backward'); });

      chgSet.append("text")
            .classed('tools', true)
            .classed('icon', true)
            .attr("text-anchor", "right")
            .attr("dx", (x/2) - 20 - 15)
            .attr("dy", function (d) { return (18-((y + (d.values.length * rowHeight))/2)); })
            .text(function(i) { return fa('forward'); });



      [1,2,3,4,5].forEach ( function (a,i) {
          chgSet.append("text")
                .classed('tools', true)
                .classed('icon', true)
                .classed('xs', true)
                .attr("text-anchor", "left")
                .attr("dx", -(x/2) + 6 + 15 + (i*10))
                .attr("dy", function (d) { return (17.5-((y + (d.values.length * rowHeight))/2)); })
                .text(function(i) { return fa('circle'); });

      });

      // page x / y
      chgSet.append("text")
            .classed('tools', true)
            .attr("text-anchor", "right")
            .attr("dx", (x/2) - 20)
            .attr("dy", function (d) { return (18-((y + (d.values.length * rowHeight))/2)); })
            .text(function(i) { return 1 + " / " + 20; });

  }
}

export default Shape;

