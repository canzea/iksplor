
import fa from 'fontawesome';
import './style.scss';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {

      const txtNode = chgSet.append("text")
          .attr("text-anchor", "left")
          .attr("dx", 12)
          .attr("dy", 4)
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .text(function(d) { return d.label; });

      chgSet.append("circle")
          .attr("r", 6)
          .style("stroke", function(d) { return d.hasOwnProperty("color") ? d.color:"grey"; })
          .style("stroke-width", ".3em");
  }
}

export default Shape;
