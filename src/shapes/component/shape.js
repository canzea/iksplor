
import * as d3 from 'd3';
import fa from 'fontawesome';
import './style.scss';

export default class {
  constructor(domainType) {
    this.domainType = domainType;
  }

  update(fullSet) {
      let cs = fullSet.filter(function (d) { return (d.flash ? true:false); });

      (function repeat() {
                cs.selectAll('rect')
                    .style('opacity',0.2)
                    .transition()
                    .duration(800)
                    .style('opacity',1)
                    .on("end", function(d){
                        d3.select(this)
                            .transition().filter(function (d) { return d.flash; })
                            .duration(800)
                            .style('opacity',0.2)
                            .on("end", repeat);
                    });
      })();



      fullSet.selectAll("rect")
          .attr("_state", function (d) {
                if (d.hasOwnProperty('state')) {
                    let previous = d3.select(this).attr("_state");
                    if (previous) {
                        d3.select(this).classed(previous, false);
                    }
                    if (d.state != "") {
                        d3.select(this).classed(d.state, true);
                    }
                    return d.state;
                } else {
                    return null;
                }
            });

  }

  build(chgSetT) {

      let w = 40, h = 40;

      let chgSet = chgSetT.append("g").attr("transform", function (d) { return (d.scale ? "scale(" + d.scale + ")":""); });

      let r = chgSet.append("rect")
          .classed("outer", true)
          .classed("inactive", function (d) { return (d.inactive ? true:false); })
          .classed("alert", function (d) { return (d.alert ? true:false); })
          .attr("rx", 6)
          .attr("x", -((w+6)/2))
          .attr("y", -((h+6)/2))
          .attr("width", w+6)
          .attr("height", h+6);


      chgSet.append("rect")
          .classed("inner", true)
          .classed("inactive", function (d) { return (d.inactive ? true:false); })
          .classed("alert", function (d) { return (d.alert ? true:false); })
          .attr("rx", 4)
          .attr("x", -(w/2))
          .attr("y", -(h/2))
          .attr("width", w)
          .attr("height", h);

      chgSet.filter(function(d) { return d.hasOwnProperty('icon'); }).append("text")
          .attr("class", "icon")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", "0.1em")
          .text(function(d) { return fa(d.icon); });

      chgSet.append("text")
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", "2.0em")
          .text(function(d) { return d.label; });

    let node = chgSet.selectAll("text,rect");
    node.on("mouseover",function(){ d3.select(this.parentNode).transition().attr("transform","scale(1.3)") });
    node.on("mouseout",function(d){ d3.select(this.parentNode).transition().attr("transform","scale(" + (d.scale ? d.scale :1) + ")") });

  }


}
