
import fa from 'fontawesome';
import './style.scss';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {
      const txtNode = chgSet.append("text")
          .attr("text-anchor", "left")
          .attr("dx", 12)
          .attr("dy", -5)
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .text(function(d) { return d.label; });

      chgSet.filter(function(d) { return d.hasOwnProperty('icon'); }).append("text")
          .attr("class", "icon")
          .attr("text-anchor", "middle")
          .style("fill", function(d) { return d.color; })
          .attr("dx", 5)
          .attr("dy", -5)
          .text(function(d) { return fa(d.icon); });

      chgSet.append("circle")
          .attr("r", 6)
          .style("stroke", function(d) { return (d.hasOwnProperty("color") ? d.color:"blue"); })
          .style("stroke-width", ".3em")
          .attr("transform", function (d) { const width = (d.hasOwnProperty("width") ? d.width : 200); return "translate(" + (width + 6) + ", 3)"; });

      chgSet.append("rect")
          .attr("class", "line")
          .style("fill", function(d) { return (d.hasOwnProperty("color") ? d.color:"blue"); })
          .attr("width", function (d) { return (d.hasOwnProperty("width") ? d.width : 200); })
          .attr("height", 6);
  }
}

export default Shape;
