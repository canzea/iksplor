
import * as d3 from "d3";
import './style.scss';
import {ViewerCanvas} from "../../ViewerCanvas";
import TextUtils from '../../utils/TextUtils';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {
      let self = this;

      chgSet.classed("tab", true);

      // Width calculated by the layout
      chgSet.append("rect")
          .attr("x", -200/2)
          .attr("y", -50/2)
          .attr("height", 50)
          .attr("width", 200);

      const textNode = chgSet.append("text")
          .attr("class", "label")
          .attr("text-anchor", "middle");

      textNode.append("tspan")
            .attr("x", 0)
            .text(function (d) { const lines = TextUtils.splitByNL(d.label); return lines[0]; });

      textNode.append("tspan")
            .attr("x", 0)
            .attr("y", function (d) { const bbox = this.parentNode.getBBox(); return bbox.height; })
            .text(function (d) { const lines = TextUtils.splitByNL(d.label); return (lines.length == 1 ? "":lines[1]); });

  }

}

export default Shape;
