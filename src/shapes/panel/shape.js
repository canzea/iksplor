
import './style.scss';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(newNodes) {
    newNodes.each(function (d) {
        if (!d.hasOwnProperty("name")) {
            alert("Panel requires a name! " + JSON.stringify(d));
        }
    });
  }
}

export default Shape;
