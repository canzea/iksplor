import * as d3 from "d3";

import './style.scss';
import fa from 'fontawesome';

import {ShapeFactory} from '../../ShapeFactory';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {
      let x = 200;
      let y = 100;

      chgSet = chgSet.append('g').attr("transform", function (d) { return "scale(" + d.scale + ")"; });

      chgSet.append("rect")
          .attr("x", -x/2)
          .attr("y", -y/2)
          .attr("width", x)
          .attr("height", y);

      chgSet.append("text")
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .attr("text-anchor", "left")
          .attr("dominant-baseline", "central")
          .attr("dx", -(x/2) + 5)
          .attr("dy", -(y/2) + 8)
          .text(function(d) { return d.label; });

      chgSet.filter(function(d) { return d.hasOwnProperty('icon'); }).append("text")
          .attr("class", "icon")
          .attr("text-anchor", "middle")
          .attr("dx", (x/2) - 12)
          .attr("dy", -(y/2) + 15)
          .text(function(d) { return fa(d.icon); });


      chgSet.filter(function (d) { return d.hasOwnProperty("inside"); })
        .each(function (e) {
            let size = e.inside.length;

            let chg = d3.select(this).selectAll('g').data(e.inside).enter().append('g').attr('class', function (c) { return c.class; })

            let location = 0;
            chg.each(function (x) {
                ShapeFactory.getShape(x.type).build(chg);
            });

        });
  }
}

export default Shape;
