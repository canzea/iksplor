
import * as d3 from 'd3';
import './style.scss';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {
      chgSet.append("image")
          .attr("x", function (d) { return -d.width/2; })
          .attr("y", function (d) { return -d.height/2; })
          .attr("width", function(d) { return d.width; })
          .attr("height", function(d) { return d.height; })
          .attr("class", function(d) { return (d.class ? d.class:"image"); })
          .attr("xlink:href", function(d) { return d.image; });
  }
}

export default Shape;
