
import * as d3 from 'd3';
import './style.scss';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {
      chgSet.append("circle")
          .attr("r", 15);

      chgSet.append("text")
          .attr("class", "iconText")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", "0.1em")
          .text(function(d) { var hex = d.title.icon.substr(3); return unescape('%u' + hex); });

      chgSet.append("text")
          .attr("class", "labelText")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", "1.6em")
          .text(function(d) { return d.title.label; });

      const build = chgSet.filter(function(d) { return d.title.label == "Build"; });
      const learn = chgSet.filter(function(d) { return d.title.label == "Learn"; });
      const measure = chgSet.filter(function(d) { return d.title.label == "Measure"; });

      build.append("line")
          .attr("class", "arrow")
          .attr("x1", 0)
          .attr("y1", 15)
          .attr("x2", 0)
          .attr("y2", 20)
      build.append("line")
          .attr("class", "arrow")
          .attr("marker-end", "url(#lifecycle)")
          .attr("x1", 15)
          .attr("y1", 0)
          .attr("x2", 30)
          .attr("y2", 0)

      measure.append("line")
          .attr("class", "arrow")
          .attr("marker-end", "url(#lifecycle)")
          .attr("x1", -15)
          .attr("y1", 0)
          .attr("x2", -30)
          .attr("y2", 0)
      measure.append("line")
          .attr("class", "arrow")
          .attr("x1", 0)
          .attr("y1", -15)
          .attr("x2", 0)
          .attr("y2", -20)

      learn.append("line")
          .attr("class", "arrow")
          .attr("x1", 15)
          .attr("y1", 0)
          .attr("x2", 20)
          .attr("y2", 0)
      learn.append("line")
          .attr("class", "arrow")
          .attr("marker-end", "url(#lifecycle)")
          .attr("x1", 0)
          .attr("y1", -15)
          .attr("x2", 0)
          .attr("y2", -30)

  }
}

export default Shape;

