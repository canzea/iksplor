import './style.scss';

import * as d3 from 'd3';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }


  update(fullSet) {
      let cs = fullSet.filter(function (d) { return (d.flash ? true:false); });

      (function repeat() {
                cs.selectAll('circle')
                    .style('opacity',0.2)
                    .transition()
                    .duration(800)
                    .style('opacity',1)
                    .on("end", function(d){
                        d3.select(this)
                            .transition().filter(function (d) { return d.flash; })
                            .duration(800)
                            .style('opacity',0.2)
                            .on("end", repeat);
                    });
      })();

      fullSet.selectAll("circle")
          .attr("_state", function (d) {
                if (d.hasOwnProperty('state')) {
                    let previous = d3.select(this).attr("_state");
                    if (previous) {
                        d3.select(this).classed(previous, false);
                    }
                    if (d.state != "") {
                        d3.select(this).classed(d.state, true);
                    }
                    return d.state;
                } else {
                    return null;
                }
            });

  }


  build(chgSetT) {

      let chgSet = chgSetT.append("g").attr("transform", function (d) { return (d.scale ? "scale(" + d.scale + ")":""); });

      chgSet.append("circle").attr("r", 25).attr("class", "circle-base");
      chgSet.append("circle").attr("r", 21).attr("class", "circle-ring");

      chgSet.filter(function (d) { return !d.hasOwnProperty("position") || d.position == "right"; })
          .append("text")
          .attr("text-anchor", "left")
          .attr("dx", function (d) { const circle = d3.select(this.parentNode).select('circle').node(); return 2 + (circle.getBBox().width/2); })
          .attr("dy", 5)
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .text(function(d) { return d.label; });

      chgSet.filter(function (d) { return d.position == "top"; })
          .append("text")
          .attr("dy", function (d) { const circle = d3.select(this.parentNode).select('circle').node(); return 2 - (circle.getBBox().height/2) - 10; })
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "central")
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .text(function(d) { return d.label; });

      chgSet.filter(function (d) { return d.position == "center"; })
          .append("text")
          .attr("text-anchor", "middle")
          .attr("dominant-baseline", "central")
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .text(function(d) { return d.label; });

    let node = chgSet.selectAll("text,circle");
    node.on("mouseover",function(){ d3.select(this.parentNode).transition().attr("transform","scale(1)") });
    node.on("mouseout",function(d){ d3.select(this.parentNode).transition().attr("transform","scale(" + (d.scale ? d.scale :1) + ")") });

/*
      chgSet.selectAll("circle").on("mouseleave", function (d) {
            d3.select(this).transition().attr('r', 20);
      });

      chgSet.selectAll("circle").on("mouseenter", function (d) {

          let cs = this;
          (function repeat() {

                    d3.select(cs)
                        .transition()
                        .duration(500)
                        .attr('r',30)
                        .on("end", function(){
                            d3.select(this)
                                .transition()
                                .duration(500)
                                .attr('r',20)
                                .on("end", repeat);
                        });
          })();
      });
      */
  }
}

export default Shape;
