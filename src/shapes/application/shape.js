
import * as d3 from 'd3';
import './style.scss';
import fa from "fontawesome";

import TextUtils from '../../utils/TextUtils';

class RectangleShape {

  constructor(domainType) {
    this.domainType = domainType;
  }


  build(chgSet) {
      var self = this;

      let x = 120;
      let y = 60;

      chgSet.append("rect")
          .attr("x", -x/2)
          .attr("y", -y/2)
          .attr("width", x)
          .attr("height", y)
          .attr("class", function (d) {
              return d.state;
          });
          //.style("transform", "translate(-50%, -50%)")
          //.style("filter", "url(#drop-shadow)")

      chgSet.append("text")
          .attr("class", "appId")
          .attr("text-anchor", "middle")
          .attr("dx", 0)
          .attr("dy", 16)
          .text(function(d) { return d.appId; });

      const appNameNode = chgSet.append("text")
          .attr("class", "label")
          .attr("text-anchor", "middle");

      appNameNode.append("tspan")
            .attr("x", 0)
            .attr("y", -4)
            .text(function (d) { const lines = TextUtils.splitByWords(d.label, 20); return lines[0]; });

      appNameNode.append("tspan")
            .attr("x", 0)
            .attr("y", 6)
            .text(function (d) { const lines = TextUtils.splitByWords(d.label, 20); return (lines.length == 1 ? "":lines[1]); });


      chgSet.filter(function (d) { return d.labels.hasOwnProperty("topLeft"); })
          .append("text")
          .attr("x", -x/2 + 2)
          .attr("y", -y/2 + 8)
          .attr("class", "topLeft")
          .attr("text-anchor", "start")
          .text(function(d) { return d.labels.topLeft; });

      chgSet.filter(function (d) { return d.labels.hasOwnProperty("topRight"); })
          .append("text")
          .attr("x", x/2 - 2)
          .attr("y", -y/2 + 8)
          .attr("class", "topRight")
          .attr("text-anchor", "end")
          .text(function(d) { return d.labels.topRight; });

      chgSet.filter(function (d) { return d.labels.hasOwnProperty("bottomLeft"); })
          .append("text")
          .attr("x", -x/2 + 2)
          .attr("y", y/2 - 4)
          .attr("class", "bottomLeft")
          .attr("text-anchor", "start")
          .text(function(d) { return d.labels.bottomLeft; });

      chgSet.filter(function (d) { return d.labels.hasOwnProperty("bottomRight"); })
          .append("text")
          .attr("x", x/2 - 2)
          .attr("y", y/2 - 4)
          .attr("class", "bottomRight")
          .attr("text-anchor", "end")
          .text(function(d) { return d.labels.bottomRight; });


      chgSet.filter(function (d) { return d.hasOwnProperty("image"); })
          .append("image")
          .attr("x", (x/2)-20-2)
          .attr("y", (y/2)-10-2)
          .attr("width", function(d) { return 20; })
          .attr("height", function(d) { return 10; })
          .attr("xlink:href", function(d) { return d.image; });

      chgSet.filter(function (d) { return (typeof d.alerts != "undefined"); }).each(function (d) {

            let alertNodes = d3.select(this).selectAll("text.alerts").data(d.alerts);

            alertNodes.enter()
                .append("text")
                      .attr("class", function (t) { return (t.class ? "icon " + t.class:"icon"); })
                      .attr("text-anchor", "end")
                      .attr("dx", function (a,i) { return (-x/2) + 15 + (i*20); } )
                      .attr("dy", (-y/2) - 2)
                      .text(function (t) { return fa(t.icon); })
                      .on("click", function (e, i) {
                            d3.event.stopPropagation();

                            alert("clicked " + i + " : " + JSON.stringify(e) + " OBJ: " + d.name);
                      });

      });
  }
}

export default RectangleShape;

