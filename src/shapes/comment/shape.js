
import fa from 'fontawesome';
import * as d3 from 'd3';

import './style.scss';
import TextUtils from '../../utils/TextUtils';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }

  build(chgSet) {
      let tmpTxtNode = chgSet.append("text")
          .attr("text-anchor", "left")
          .attr("dx", 0)
          .attr("dy", 0)
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .text(function (d) { return d.label; });

      let bbox = [];
      tmpTxtNode.each(function (d,i) { bbox[i] = this.getBBox(); });

      const txtNode = chgSet.append("text")
          .attr("text-anchor", "left")
          .attr("dx", 0)
          .attr("dy", 0)
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .each(function(d, ti) {
              d3.select(this).selectAll('tspan').data(TextUtils.splitByWidth(d.label, bbox[ti].width, (d.width ? d.width:999999)))
              .enter().append("tspan")
                    .attr("x", 0)
                    .attr("y", function (d, i) { return (i*bbox[ti].height); })
                    .text(function (line) { return line; });
          });

      tmpTxtNode.each(function (d) { this.parentNode.removeChild(this); });

      chgSet.filter(function(d) { return d.hasOwnProperty('icon'); }).append("text")
          .attr("class", "icon")
          .attr("text-anchor", "middle")
          .attr("dx", -10)
          .attr("dy", 0)
          .text(function(d) { return fa(d.icon); });


  }
}

export default Shape;
