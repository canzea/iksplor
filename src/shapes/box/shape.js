
import './style.scss';
import fa from 'fontawesome';
import TextUtils from '../../utils/TextUtils';

import * as d3 from 'd3';

class Shape {
  constructor(domainType) {
    this.domainType = domainType;
  }


  update(fullSet) {
      let cs = fullSet.filter(function (d) { return (d.flash ? true:false); });

      (function repeat() {
                cs.selectAll('rect')
                    .style('opacity',0.2)
                    .transition()
                    .duration(800)
                    .style('opacity',1)
                    .on("end", function(d){
                        d3.select(this)
                            .transition().filter(function (d) { return d.flash; })
                            .duration(800)
                            .style('opacity',0.2)
                            .on("end", repeat);
                    });
      })();

      fullSet.selectAll("rect")
          .attr("_state", function (d) {
                if (d.hasOwnProperty('state')) {
                    let previous = d3.select(this).attr("_state");
                    if (previous) {
                        d3.select(this).classed(previous, false);
                    }
                    if (d.state != "") {
                        d3.select(this).classed(d.state, true);
                    }
                    return d.state;
                } else {
                    return null;
                }
            });

  }

  build(_chgSet) {
      let x = 140;
      let y = 50;

      let chgSet = _chgSet.append('g').attr("transform", function (d) { return (d.hasOwnProperty('scale') ? "scale("+d.scale+")":""); });

      chgSet.append("rect")
          .attr("x", -x/2)
          .attr("y", -y/2)
          .attr("width", x)
          .attr("height", y);

      const textNode = chgSet.append("text")
          .attr("class", function(d) { return (d.class ? d.class:"label"); })
          .attr("dominant-baseline", "central")
          .attr("text-anchor", "middle");

      textNode.append("tspan")
            .attr("x", 0)
            .attr("y", function (d) { const bbox = this.parentNode.getBBox(); return -bbox.height/2 - 12; })
            .text(function (d) { const bbox = this.parentNode.getBBox(); const lines = TextUtils.splitByWidth(d.label, 140, bbox.width); return lines[0]; });

      textNode.append("tspan")
            .attr("x", 0)
            .attr("y", function (d) { const bbox = this.parentNode.getBBox(); return -bbox.height/2 + 23; })
            .text(function (d) { const bbox = this.parentNode.getBBox(); const lines = TextUtils.splitByWidth(d.label, 140, bbox.width); return (lines.length == 1 ? "":lines[1]); });

      chgSet.filter(function(d) { return d.hasOwnProperty('icon'); }).append("text")
          .attr("class", "icon")
          .attr("text-anchor", "middle")
          .attr("dx", (x/2) - 12)
          .attr("dy", -(y/2) + 20)
          .text(function(d) { return fa(d.icon); });

  }
}

export default Shape;
