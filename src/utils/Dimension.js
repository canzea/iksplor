

class Dimension {
  constructor(crossfilter, func) {
       this.dimension = crossfilter.dimension(func);
       this.dimensionMirror = crossfilter.dimension(func);
  }

  /*
    Build and return the shape that supports the dimension
  */
  buildShape(label) {
      let nodeData = this.dimension.group().top(Infinity);
      nodeData.forEach(function (d) {
            d.id = d.key;
            d.label = d.key;
            d.count = d.value;
            d.selected = false;
            d.noSelections = true;
      });
      this.data = nodeData;

      return {"type":"list", "boundary":"A", "label":label, "values":nodeData};
  }

  /*
    Manage the selected values, so when the selection changes

    When an list item is selected, it should result in a new filter created
  */

  applyFilter () {
       let filt = [];
       this.data.forEach (function (d) {
           if (d.selected) {
                filt.push(d.key);
           }
       });
       if (filt.length == 0) {
          this.dimension.filterAll();
       } else {
           this.dimension.filter(function (d) { return filt.includes(d); });
       }
       this.filters = filt;
  }

  /*
     Need to update the counts based on the new data
  */
  updateShape () {
       const self = this;
       let counts = this.dimensionMirror.group().top(Infinity);
       let countMap = {};
       counts.forEach(function (d) {
          countMap[d.key] = d.value;
       });
       this.data.forEach (function (d) {
          if (countMap.hasOwnProperty(d.key)) {
              d.count = countMap[d.key];
          } else {
              d.count = 0;
          }
          d.noSelections = (self.filters.length == 0);
       });
  }

  values (top = Infinity) {
     return this.dimension.top(top);
  }
}

export default Dimension;
