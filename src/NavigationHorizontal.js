

import * as d3 from "d3";

class Navigation {

  attach (svg, viewer, contentNode) {
     let self = this;
     this.svg = svg;
     this.contentNode = contentNode;
     this.viewer = viewer;

     svg.append("g").attr("class", "nav");
     this.state = { "mode":"closed", "selected":-1 };

     this.menuItems = [];

     viewer.on("closeNavigation", function (e) {
        self.close();
     });
  }

  addMenuItem (item) {
     this.menuItems.push(item);
  }

  update () {
      let self = this;
      let vis = this.svg;


      this.root = vis.select("g.nav").append("g");

      this.backdrop = vis.select("g.nav").select("g").append("rect")
        .attr("fill", "#999999")
        .attr("x", 0)
        .attr("y", 0)
        .attr("rx", 6)
        .attr("ry", 6)
        .attr("width", "280")
        .attr("height", "80")
        .style("visibility", "hidden");

      const menuNodes = vis.select("g.nav").select("g").selectAll("g.navDetail")
                                .data(this.menuItems);

      const navNodes = menuNodes.enter()
          .append("g")
          .attr("class", "navDetail")
          .style("visibility", "hidden");

      menuNodes.exit().remove();

      navNodes.append("rect")
        .attr("fill", "white")
        .attr("x", 2)
        .attr("y", 4)
        .attr("rx", 6)
        .attr("ry", 6)
        .attr("transform", "rotate(35)")
        .attr("width", "55")
        .attr("height", "12");

      navNodes.append("circle")
        .style("cursor", "default")
        .attr("fill", "black")
        .attr("stroke", "white")
        .attr("stroke-width", "2")
        .attr("r", "10");

      navNodes.append("text")
        .style("cursor", "default")
        .attr("dx", 0)
        .attr("dy", "0")
        .attr("text-anchor", "middle")
        .attr('dominant-baseline', 'central')
        .style('font-family', 'FontAwesome')
        .style('font-size', '10px')
        .style("fill", "white")
        .text(function(d) {return d.icon; });

      navNodes.append("text")
        .style("cursor", "default")
        .attr("dx", 8)
        .attr("dy", 12)
        .attr("fill", "black")
        .attr("text-anchor", "left")
        .style('font-size', '0.4em')
        .attr("transform", "rotate(35)")
        .text(function(d) { return d.title; });

      this.backdrop
        .on('click', function (e) {
            self.close();
        });

      navNodes
        .on('click', function (e) {
            self.viewer.trigger("shapeNavClick", {"data":self.state.data, "menuItem":e});
            //self.close();
        });

      navNodes.selectAll('circle,text,rect')
        .on('mouseenter', function (nodeSet, index) {
            d3.select(this.parentNode).selectAll('circle,rect').classed("nav-hover", true);
        })
        .on('mouseleave', function (e) {
            d3.select(this.parentNode).selectAll('circle,rect').classed("nav-hover", false);
//            vis.select(this).selectAll('circle').removeClass("nav-hover");
        });
  }

  toggle (e, matchedItem) {
      let self = this;
      let pos = null;//{x:e.x,y:e.y};
      let state = self.state;

      if (typeof state.newNode != "undefined" && state.newNode != null) {
          //console.log("REMOVING: " + state.newNode);
          state.newNode.parentNode.removeChild(state.newNode);
          state.newNode = null;
      }

      let notes = [];
      if (matchedItem) {
        matchedItem.each(function (d) {
          let bounded = this.getBBox();
          self.state.newNode = this.cloneNode(true);

          self.svg.select("g.nav").node().appendChild(self.state.newNode);
          d3.select(self.state.newNode)
            .on('click', function (e) {
                self.close();
          });

          pos = {left:e.x + bounded.x, top:e.y + bounded.y, width:bounded.width, height:bounded.height, bounding:this.getBoundingClientRect()};

          notes = ( d.hasOwnProperty('notes') ? d.notes : [] );
          self.state.data = d;
        });
      }
      if (this.state.mode == "closed") {
        this.doOpen(e, pos, notes);
      } else {
        if (this.state.selected != e.index) {
          this.doOpen(e, pos, notes);
        } else {
          this.close(e);
        }
      }
  }

  open (e) {
    this.doOpen (e, {x:e.x, y:e.y});
  }

  doOpen (e, position, notes) {
      const self = this;
      this.state.x = position.left;
      this.state.y = position.top;
      this.state.selected = e.index;
      this.state.mode = "open";

      let scale = 1.6;
      let delta = 0;

      const start = 198;
      // calculate positions of menu items
      let positions = [0];
      for ( let i = 0; i < 10; i++) {
        if ((start+(36*i)) > 360) {
          positions.push(start + (36 * i) - 360);
        } else {
          positions.push(start + (36 * i) );
        }
      }

      let vis = this.svg;

      // Position the navigation in the right place
      vis.select("g.nav").selectAll('g.node')
        //.attr("transform", "translate(0,0)")
        .style("pointer-events", "none");

      vis.select("g.nav").select("g")
        .attr("transform", function(d) {
           return "translate(" + (position.left + (position.width/2)) + "," + (position.top + (position.height/2)) + ")";
        });
      vis.select("g.nav").select('rect')
        .attr("opacity", 0)
        .style("visibility", "visible")
        .attr("width", function (d) {
          return Math.max(position.width, 150) + 10;
        })
        .attr("height", function (d) {
          return position.height  + 10;
        })
        .attr("transform", function(d) {
          return "translate(" + ((-position.width/2) - 5) + "," + ((-position.height/2) - 5) + ")";
        });


      // Determine how big the height should be based on the "notes" that are provided
      // Move a navigation DIV to the same position for displaying
      let element = this.contentNode.parentNode;
      let bodyRect = document.body.getBoundingClientRect(),
          elemRect = element.getBoundingClientRect(),
          offset   = elemRect.top - bodyRect.top;

      let svgBB = self.svg.node().getBoundingClientRect();
      this.printRect(svgBB, "svg BCR");

      let svgParentBB = self.svg.node().parentNode.getBoundingClientRect();
      this.printRect(svgParentBB, "SVG Parent BCR");

      let navBB = vis.select("g.nav").select('rect').node().getBoundingClientRect();
      this.printRect(navBB, "NAV BCR");

      this.printRect(position, "obj");
      this.printRect(position.bounding, "obj BCR");
      this.printRect(elemRect, "nav BCR");
      this.printRect(bodyRect, "body BCR");

      console.log("scrollTop : " + document.body.scrollTop);
      let minHeight = Math.max(position.bounding.height + 25, (self.menuItems == 0 ? 50 : 145));
      let minWidth = navBB.width ;

      d3.select(this.contentNode)
            .classed("content", true)
            .style("display", "block")
            .style("position", "absolute")
            .style("width", "" + (minWidth - 10) + "px")
            .style("left", ""+ ((navBB.left - svgParentBB.left) + 5) + "px")
            .style("top", "" + ((navBB.top - svgParentBB.top) + navBB.height)  + "px");
            //.style("left", ""+ (position.bounding.left) + "px")
            //.style("top", "" + (-bodyRect.top + position.bounding.top + minHeight)  + "px");

// + position.bounding.top
      d3.select(this.contentNode)
           .selectAll("table")
           .remove();

      let noteNode = d3.select(this.contentNode).append("table")
                            .selectAll("tr")
                            .data(notes);
      noteNode.enter()
           .append("tr")
               .html(function(d) { if (typeof d == "string") { return d; } else {
                    return "<td class='key'>" + d.key + "</td><td class='value'>" + d.value + "</td>";
               }});


      let dims = this.contentNode.getBoundingClientRect();
      this.printRect(dims);

      let offset2 = dims.top - elemRect.top;


      //console.log("OFFSET = "+offset2+", "+dims.top);
      let contentHeight = dims.height;

      let ratio = position.height / position.bounding.height;

      // Adjust the height using the same ratio between boundingrect and BBBox
      vis.select("g.nav").select('rect')
        .attr("height", function (d) {
          return 0 + (navBB.height + (contentHeight * ratio));
        })
        .attr("width", function (d) {
          return Number(d3.select(this).attr('width')) + (self.menuItems.length == 0 ? 0:52) + (self.menuItems.length * 38);
        })
        .transition()
          .attr("opacity", 0.9).on("end", function() { return; });

      let menuPosition = 0;
      vis.select("g.nav").selectAll('g.navDetail')
        .style("opacity", 0)
        .style("visibility", "visible")
        .attr("transform", function(d) {
           return "translate(" + 0 + "," + (20 + delta) + ") scale(" + scale + ")";
        })
        .transition()
          .style("opacity", 100)
          .attr("transform", function(d) {
           const y = (-position.height/2) + 20 + delta;
           const x = (position.width/2) + ((22+1) * (menuPosition++) * scale) + 25;
           return "translate(" + x + "," + y + ") scale(" + scale + ")";
        });

  }

  close () {
      let state = this.state;
      let vis = this.svg;

      if (state.mode == "closed") {
        return;
      }
      state.mode = "closed";

      d3.select(this.contentNode)
            .style("display", "none");

      vis.select("g.nav").selectAll('g.navDetail')
        .transition()
        .style("opacity", 0)
        .attr("transform", function() { return "translate(" + state.x + "," + (state.y + 40) +")";  } )
        .on("end", function() { vis.select("g.nav").selectAll('g.navDetail').style("visibility", "hidden"); } );

      if (state.newNode) {
          //console.log("REMOVING: " + state.newNode);
          state.newNode.parentNode.removeChild(state.newNode);
          state.newNode = null;
          state.data = null;
      }

      vis.select("g.nav").select('rect')
        .transition()
        .attr("opacity", 0)
        .on("end", function() { vis.select("g.nav").select('rect').style("visibility", "hidden"); } );
  }


  printRect(elemRect, label="E") {
     console.log(label + ": Top=" + elemRect.top+",Left="+elemRect.left+",Width="+elemRect.width+",Height="+elemRect.height);
  }
}

export default Navigation;
